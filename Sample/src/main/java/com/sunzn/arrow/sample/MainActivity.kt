package com.sunzn.arrow.sample

import android.os.Bundle
import android.widget.Button
import android.widget.SeekBar
import androidx.appcompat.app.AppCompatActivity
import com.sunzn.arrow.library.ArrowView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val arrow = findViewById<ArrowView>(R.id.arrow)
        val seek = findViewById<SeekBar>(R.id.seek)
        seek.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                arrow.setValue(progress)
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {
                // TODO
            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {
                // TODO
            }

        })
        findViewById<Button>(R.id.button).setOnClickListener { menu() }
    }

    private fun menu() {
        DrawerFrame.show(supportFragmentManager, object : DrawerFrame.ActionListener {
            override fun onRemove() {}
            override fun onCancel() {}
        })
    }

}