package com.sunzn.arrow.sample

import android.content.res.Resources
import android.os.Bundle
import android.util.Log
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.FragmentManager
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.sunzn.arrow.sample.databinding.FrameUndoBinding
import com.sunzn.frame.library.Drawer
import kotlin.math.absoluteValue

class DrawerFrame : Drawer(), View.OnClickListener {

    private var actionListener: ActionListener? = null
    private lateinit var binding: FrameUndoBinding

    fun setActionListener(listener: ActionListener?): DrawerFrame {
        actionListener = listener
        return this
    }

    override fun initContentView(inflater: LayoutInflater, container: ViewGroup?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.frame_undo, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
    }

    private fun init() {
        initBind()
    }

    private fun initBind() {
        binding.onClickListener = this
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.menu_action -> {
                if (actionListener != null) actionListener?.onRemove()
                dismissAllowingStateLoss()
                return
            }

            R.id.menu_cancel -> {
                if (actionListener != null) actionListener?.onCancel()
                dismissAllowingStateLoss()
            }
        }
    }

    override fun onDrawerStateChanged(drawer: View, newState: Int) {
        when (newState) {
            BottomSheetBehavior.STATE_HIDDEN -> {
                Log.e("UndoFrame", "STATE_HIDDEN")
            }

            BottomSheetBehavior.STATE_EXPANDED -> {
                Log.e("UndoFrame", "STATE_EXPANDED")
            }

            BottomSheetBehavior.STATE_COLLAPSED -> {
                Log.e("UndoFrame", "STATE_COLLAPSED")
            }

            BottomSheetBehavior.STATE_DRAGGING -> {
                Log.e("UndoFrame", "STATE_DRAGGING")
            }

            BottomSheetBehavior.STATE_SETTLING -> {
                Log.e("UndoFrame", "STATE_SETTLING")
            }
        }
    }

    override fun onDrawerSlide(drawer: View, slideOffset: Float) {
        Log.e("UndoFrame", "slideOffset = $slideOffset")
        binding.menuArrow.setValue((3.dp * slideOffset * 3).toInt().absoluteValue)
        // binding.menuArrow.setRatio(slideOffset.absoluteValue)
    }

    interface ActionListener {
        fun onRemove()
        fun onCancel()
    }

    companion object {
        fun show(manager: FragmentManager?, listener: ActionListener?) {
            DrawerFrame().setActionListener(listener).setCancel(true).setDimAmount(0.3f).show(manager)
        }
    }

    private val Float.dp: Float get() = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, this, Resources.getSystem().displayMetrics)

    private val Int.dp: Int get() = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, this.toFloat(), Resources.getSystem().displayMetrics).toInt()

}