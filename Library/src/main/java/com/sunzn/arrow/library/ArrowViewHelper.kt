package com.sunzn.arrow.library

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Path
import android.util.AttributeSet
import android.view.View
import androidx.core.content.withStyledAttributes
import kotlin.math.min
import kotlin.math.pow
import kotlin.math.sqrt

class ArrowViewHelper(private val view: ArrowView, context: Context, attrs: AttributeSet?, defStyleAttr: Int) {

    interface MeasureListener {
        fun onSetMeasuredDimension(width: Int, height: Int)
    }

    companion object Config {
        private const val DEFAULT_SIDE_LINE_WIDTH = 40
        private const val DEFAULT_SIDE_LINE_THICK = 10
        private const val DEFAULT_SLIP_DOWN_DEPTH = 8
        private const val DEFAULT_FILL_LINE_COLOR = Color.GRAY
    }

    private var sideLineWidth: Int = DEFAULT_SIDE_LINE_WIDTH
    private var sideLineThick: Int = DEFAULT_SIDE_LINE_THICK
    private var slipDownDepth: Int = DEFAULT_SLIP_DOWN_DEPTH
    private var fillLineColor: Int = DEFAULT_FILL_LINE_COLOR

    private var width: Int = 0
    private var height: Int = 0
    private var depth = 0
    private var path = Path()

    private val linePaint by lazy {
        Paint(Paint.ANTI_ALIAS_FLAG).also {
            it.style = Paint.Style.STROKE
            it.color = fillLineColor
            it.strokeWidth = sideLineThick.toFloat()
            it.strokeCap = Paint.Cap.ROUND
        }
    }

    init {
        context.withStyledAttributes(attrs, R.styleable.ArrowView) {
            sideLineWidth = getDimensionPixelSize(R.styleable.ArrowView_arrow_side_line_width, DEFAULT_SIDE_LINE_WIDTH)
            sideLineThick = getDimensionPixelSize(R.styleable.ArrowView_arrow_side_line_thick, DEFAULT_SIDE_LINE_THICK)
            slipDownDepth = getDimensionPixelSize(R.styleable.ArrowView_arrow_slip_down_depth, DEFAULT_SLIP_DOWN_DEPTH)
            fillLineColor = getColor(R.styleable.ArrowView_arrow_fill_line_color, DEFAULT_FILL_LINE_COLOR)
        }
    }

    fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int, listener: MeasureListener) {
        width = View.MeasureSpec.getSize(widthMeasureSpec)
        height = slipDownDepth * 2 + view.paddingTop + view.paddingBottom
        listener.onSetMeasuredDimension(width, height)
    }

    fun onDraw(canvas: Canvas?) {
        path.reset()
        path.moveTo(initStartX(), initStartY())
        path.lineTo(initCenterX(), initCenterY())
        path.rLineTo(initEndX(), initEndY())
        canvas?.drawPath(path, linePaint)
    }

    fun setValue(space: Int) {
        depth = min(space, slipDownDepth)
        view.invalidate()
    }

    fun setRatio(ratio: Float) {
        depth = (min(ratio, 1F) * slipDownDepth).toInt()
        view.invalidate()
    }

    private fun initStartX(): Float {
        return width / 2F - sqrt((sideLineWidth / 2F).pow(2) - (depth / 2F).pow(2)) * 2F
    }

    private fun initStartY(): Float {
        return (view.paddingTop + slipDownDepth - depth).toFloat()
    }

    private fun initCenterX(): Float {
        return width / 2F
    }

    private fun initCenterY(): Float {
        return (view.paddingTop + slipDownDepth + depth).toFloat()
    }

    private fun initEndX(): Float {
        return sqrt((sideLineWidth / 2F).pow(2) - (depth / 2F).pow(2)) * 2F
    }

    private fun initEndY(): Float {
        return depth * -2F
    }

}