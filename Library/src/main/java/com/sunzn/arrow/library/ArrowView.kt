package com.sunzn.arrow.library

import android.content.Context
import android.graphics.Canvas
import android.util.AttributeSet

class ArrowView @JvmOverloads constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int = 0) : ArrowBaseView(context, attrs, defStyleAttr) {

    private val helper: ArrowViewHelper = ArrowViewHelper(this, context, attrs, defStyleAttr)

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        helper.onMeasure(widthMeasureSpec, heightMeasureSpec, object : ArrowViewHelper.MeasureListener {
            override fun onSetMeasuredDimension(width: Int, height: Int) {
                setMeasuredDimension(width, height)
            }
        })
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        helper.onDraw(canvas)
    }

    fun setValue(space: Int) {
        helper.setValue(space)
    }

    fun setRatio(ratio: Float) {
        helper.setRatio(ratio)
    }

}