# arrow

[![](https://jitpack.io/v/com.sunzn/arrow.svg)](https://jitpack.io/#com.sunzn/arrow)
[![](https://img.shields.io/badge/License-Apache%202.0-orange.svg)](http://www.apache.org/licenses/LICENSE-2.0.html)

An animated arrow component.

### ScreenShot

<img src="screen/001.gif" width="50%">

### Gradle

#### 1. Add the JitPack repository to your build file.

```groovy
allprojects {
    repositories {
        ...
        maven { url 'https://jitpack.io' }
    }
 }
```

#### 2. Add the dependency.

```groovy
dependencies {
    implementation 'com.sunzn.arrow:arrow:1.0.0'
}
```

### Use
```groovy
<com.sunzn.arrow.library.ArrowView
    android:id="@+id/menu_arrow"
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    android:paddingVertical="15dp"
    app:arrow_fill_line_color="@android:color/holo_red_dark"
    app:arrow_side_line_thick="5dp"
    app:arrow_side_line_width="18dp"
    app:arrow_slip_down_depth="3dp" />
```

| Attribute              | Describe                                       | Additional                           |
|------------------------|------------------------------------------------| ------------------------------------ |
| arrow_side_line_width  | The width of the line on one side of the arrow | Support dimension and reference      |
| arrow_side_line_thick  | The thickness of arrow lines                   | Support dimension and reference      |
| arrow_slip_down_depth  | The max distance that arrow can slide          | Support dimension and reference      |
| arrow_fill_line_color  | The color of the arrow                         | Support color                        |

### License
```
    Copyright [2023-2023] sunzn

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
```
